// axios
import axios from 'axios'

const baseURL = `${process.env.VUE_APP_SERVER_URL}api/`

export default axios.create({
  baseURL: baseURL,
  // You can add your headers here
})
