/*=========================================================================================
  File Name: sidebarItems.js
  Description: Sidebar Items list. Add / Remove menu items from here.
  Strucutre:
          url     => router path
          name    => name to display in sidebar
          slug    => router path name
          icon    => Feather Icon component/icon name
          tag     => text to display on badge
          tagColor  => class to apply on badge element
          i18n    => Internationalization
          submenu   => submenu of current item (current item will become dropdown )
                NOTE: Submenu don't have any icon(you can add icon if u want to display)
          isDisabled  => disable sidebar item/group
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

export default [
  {
    header: "สำหรับผู้จัดการและหัวหน้า",
    items: [
      {
        url: "/MenuForManager",
        name: "หน้าหลัก",
        slug: "MenuForManager",
        icon: "GridIcon",
      },
      {
        url: "/ManageEmployee",
        name: "จัดการพนักงาน",
        slug: "จัดการพนักงาน",
        icon: "UserIcon",
      },
      {
        url: "/ManageFoodMenu",
        name: "จัดการรายการอาหาร",
        slug: "จัดการรายการอาหาร",
        icon: "ListIcon",
      },
      {
        url: "/ManageTable",
        name: "จัดการโต๊ะ",
        slug: "จัดการโต๊ะ",
        icon: "PackageIcon",
      },
      {
        url: "/attend",
        name: "แคชเชียร์",
        slug: "แคชเชียร์",
        icon: "ArchiveIcon",
      },
      {
        url: "/Report",
        name: "รายงาน",
        slug: "รายงาน",
        icon: "CalendarIcon",
      },
      {
        url: "/Setting",
        name: "ตั้งค่า",
        slug: "ตั้งค่า",
        icon: "SettingsIcon",
      }
    ]

  },
  {
    header: 'สำหรับผู้จัดการและพนักงาน',
    items: [
      {
        url: '/OrderTable',
        name: "บริการลูกค้า",
        slug: "บริการลูกค้า",
        icon: "SmileIcon",
      }
    ]
  },
  {
    header: 'สำหรับครัว',
    items: [
      {
        url: '/Kitchen',
        name: "รายการอาหารที่ได้รับ",
        slug: "รายการอาหารที่ได้รับ",
        icon: "PlusCircleIcon",
      }
    ]
  },
  {
    header: 'สำหรับแคชเชียร์',
    items: [
      {
        url: '/Cashier',
        name: "ชำระเงิน",
        slug: "ชำระเงิน",
        icon: "InfoIcon",
      }
    ]
  }
  
  
];
