import Vue from "vue"
import { AclInstaller, AclCreate, AclRule } from "vue-acl"
import router from "@/router"

Vue.use(AclInstaller)

let initialRole = "editor"

let userInfo = JSON.parse(localStorage.getItem("userInfo"))
if(userInfo && userInfo.userRole) initialRole = userInfo.userRole


export default new AclCreate({
  initialRole: initialRole,
  router,
  acceptLocalRules: true,
  globalRules: {
    admin: new AclRule("admin").generate(),
    editor: new AclRule("editor").generate(),
    ผู้จัดการ: new AclRule("ผู้จัดการ").or("หัวหน้า").generate(),
    ครัว: new AclRule("ครัว").generate(),
    พนักงาน: new AclRule("พนักงาน").generate(),
    หัวหน้า: new AclRule("พนักงาน").or("ผู้จัดการ").generate(),
    แคชเชียร์: new AclRule("แคชเชียร์").generate(),
    พนักงานและผู้จัดการ: new AclRule("พนักงาน").or("ผู้จัดการ").or("หัวหน้า").generate(),
    ทุกคน: new AclRule("ผู้จัดการ").or("ครัว").or("พนักงาน").or("หัวหน้า").or("แคชเชียร์").generate()
    // public: new AclRule("public").or("admin").or("editor").generate(),
  }
  
})
