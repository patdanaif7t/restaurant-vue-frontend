/*=========================================================================================
  File Name: router.js
  Description: Routes for vue-router. Lazy loading is enabled.
  Object Strucutre:
                    path => router path
                    name => router name
                    component(lazy loading) => component to load
                    meta : {
                      rule => which user can have access (ACL)
                      breadcrumb => Add breadcrumb to specific page
                      pageTitle => Display title besides breadcrumb
                    }
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

import Vue from "vue";
import Router from "vue-router";
import auth from "@/auth/authService";

import "firebase/auth";

import store from "@/store/store.js";

Vue.use(Router);

const router = new Router({
  mode: "history",
  base: process.env.BASE_URL,
  scrollBehavior() {
    return { x: 0, y: 0 };
  },
  routes: [
    {
      // =============================================================================
      // MAIN LAYOUT ROUTES
      // =============================================================================
      path: "",
      component: () => import("./layouts/main/Main.vue"),
      children: [
        // =============================================================================
        // Theme Routes
        // =============================================================================

        // MANAGERRRRRRRRRRRRRRRRRRRRRRRRRRR
        {
          path: "/",
          redirect: "/dashboard/analytics"
        },
        {
          path: "/dashboard/analytics",
          name: "dashboard-analytics",
          component: () => import("./views/DashboardAnalytics.vue"),
          meta: {
            rule: "ผู้จัดการ"
          }
        },
        {
          path: "/dashboard/bill",
          name: "dashboard-bill",
          component: () => import("./views/analytics/DataBill.vue"),
          meta: {
            rule: "ผู้จัดการ"
          }
        },
        {
          path: "/dashboard/cancel",
          name: "dashboard-cancel",
          component: () => import("./views/analytics/DataCancel.vue"),
          meta: {
            rule: "ผู้จัดการ"
          }
        },
        {
          path: "/MenuForManager",
          name: "หน้าหลัก",
          component: () => import("./views/menu-manager/homeManager.vue"),
          meta: {
            rule: "ผู้จัดการ"
          }
        },
        {
          path: "/ManageEmployee",
          name: "จัดการพนักงาน",
          component: () => import("./views/employee/DataEmployee.vue"),
          meta: {
            rule: "ผู้จัดการ",
            required: true
          }
        },
        {
          path: "/ManageFoodMenu",
          name: "จัดการรายการอาหาร",
          component: () => import("./views/food-menu/DataFoodMenu.vue"),
          meta: {
            rule: "ผู้จัดการ"
          }
        },
        {
          path: "/ManageTable",
          name: "จัดการโต๊ะ",
          component: () => import("./views/table/DataTable.vue"),
          meta: {
            rule: "ผู้จัดการ"
          }
        },
        {
          path: "/Report",
          name: "รายงาน",
          component: () => import("./views/DashboardAnalytics.vue"),
          meta: {
            rule: "ผู้จัดการ"
          }
        },
        {
          path: "/Setting",
          name: "ตั้งค่า",
          component: () => import("./views/store-setting/store-setting.vue"),
          meta: {
            rule: "ผู้จัดการ"
          }
        },
        {
          path: "/OrderTable",
          name: "บริการลูกค้า",
          component: () => import("./views/order-table/DataOrderTable.vue"),
          meta: {
            rule: "พนักงานและผู้จัดการ"
          }
        },
        {
          path: "/OrderMenu",
          name: "สั่งอาหาร",
          component: () => import("./views/order-table/DataOrderMenuTable.vue"),
          meta: {
            rule: "พนักงานและผู้จัดการ"
          }
        },
        {
          path: "/OrderCheck",
          name: "ตรวจสอบรายการสั่งซื้อ",
          component: () => import("./views/order-table/DataOrderCheck.vue"),
          meta: {
            rule: "พนักงานและผู้จัดการ"
          }
        },
        {
          path: "/OrderStatus",
          name: "ตรวจสอบรายการอาหาร",
          component: () => import("./views/order-table/DataOrderStatus.vue"),
          meta: {
            rule: "พนักงานและผู้จัดการ"
          }
        },
        {
          path: "/Kitchen",
          name: "รายการอาหารที่ได้รับ",
          component: () => import("./views/kitchen/DataKitchenTable.vue"),
          meta: {
            rule: "ครัว"
          }
        },
        {
          path: "/Kitchen/Accept",
          name: "รายการอาหารที่ได้รับ",
          component: () => import("./views/kitchen/DataKitchenHistory.vue"),
          meta: {
            rule: "ครัว"
          }
        },
        {
          path: "/Cashier",
          name: "สำหรับแคชเชียร์",
          component: () => import("./views/cashier/DataCashier.vue"),
          meta: {
            rule: "แคชเชียร์"
          }
        },
        {
          path: "/attend",
          name: "จัดการเข้างานแคชเชียร์",
          component: () => import("./views/cashier/CashierAttend.vue"),
          meta: {
            rule: "ผู้จัดการ"
          }
        },
        {
          path: "/Payment",
          name: "หน้าชำระเงิน",
          component: () => import("./views/cashier/Payment.vue"),
          meta: {
            rule: "แคชเชียร์"
          }
        },
        {
          path: "/ChangePassword",
          name: "เปลี่ยนรหัสผ่าน",
          component: () => import("./views/changepassword/changepassword.vue"),
          meta: {
            rule: "ทุกคน"
          }
        },
      ]
    },
    // =============================================================================
    // FULL PAGE LAYOUTS
    // =============================================================================
    {
      path: "",
      component: () => import("@/layouts/full-page/FullPage.vue"),
      children: [
        // =============================================================================
        // PAGES
        // =============================================================================
        {
          path: "/callback",
          name: "auth-callback",
          component: () => import("@/views/Callback.vue"),
          meta: {
            rule: "editor"
          }
        },
        {
          path: "/pages/login",
          name: "page-login",
          component: () => import("@/views/pages/login/Login.vue"),
          meta: {
            rule: "editor"
          }
        },
        {
          path: "/pages/register",
          name: "page-register",
          component: () => import("@/views/pages/register/Register.vue"),
          meta: {
            rule: "editor"
          }
        },
        {
          path: "/pages/forgot-password",
          name: "page-forgot-password",
          component: () => import("@/views/pages/ForgotPassword.vue"),
          meta: {
            rule: "editor"
          }
        },
        {
          path: "/pages/reset-password",
          name: "page-reset-password",
          component: () => import("@/views/pages/ResetPassword.vue"),
          meta: {
            rule: "editor"
          }
        },
        {
          path: "/pages/lock-screen",
          name: "page-lock-screen",
          component: () => import("@/views/pages/LockScreen.vue"),
          meta: {
            rule: "editor"
          }
        },
        {
          path: "/pages/comingsoon",
          name: "page-coming-soon",
          component: () => import("@/views/pages/ComingSoon.vue"),
          meta: {
            rule: "editor"
          }
        },
        {
          path: "/pages/error-404",
          name: "page-error-404",
          component: () => import("@/views/pages/Error404.vue"),
          meta: {
            rule: "editor"
          }
        },
        {
          path: "/pages/error-500",
          name: "page-error-500",
          component: () => import("@/views/pages/Error500.vue"),
          meta: {
            rule: "editor"
          }
        },
        {
          path: "/pages/not-authorized",
          name: "page-not-authorized",
          component: () => import("@/views/pages/NotAuthorized.vue"),
          meta: {
            rule: "editor"
          }
        },
        {
          path: "/pages/maintenance",
          name: "page-maintenance",
          component: () => import("@/views/pages/Maintenance.vue"),
          meta: {
            rule: "editor"
          }
        }
      ]
    },
    // Redirect to 404 page, if no match found
    {
      path: "*",
      redirect: "/pages/error-404"
    }
  ]
});

router.afterEach(() => {
  // Remove initial loading
  const appLoading = document.getElementById("loading-bg");
  if (appLoading) {
    appLoading.style.display = "none";
  }
});

router.beforeEach((to, from, next) => {
  // if (
  //     to.path === "/pages/login" ||
  //     to.path === "/pages/forgot-password" ||
  //     to.path === "/pages/error-404" ||
  //     to.path === "/pages/error-500" ||
  //     to.path === "/pages/register" ||
  //     to.path === "/callback" ||
  //     to.path === "/pages/comingsoon" ||
  //     (auth.isAuthenticated() || firebaseCurrentUser)
  // ) {
  //     return next();
  // }

  // If auth required, check login. If login fails redirect to login page
  // if(to.meta.authRequired) {
  //   if (!(auth.isAuthenticated())) {
  //     router.push({ path: '/pages/login', query: { to: to.path } })
  //   }
  // }

  // return next()
  // Specify the current path as the customState parameter, meaning it
  // will be returned to the application after auth
  // auth.login({ target: to.path });

  if (to.path === "/ChangePassword") {
    next();
  } else {
    if (!auth.isAuthenticated()) {
      // ยังไม่ได้ล็อคอิน
      if (to.path !== "/pages/login") {
        // ถ้าไม่ได้เข้าหน้าล็อคอินให้ไปที่หน้าล็อคอิน
        next({ path: "/pages/login" });
      } else {
        next();
      }
    } else {
      // ล็อคอินสำสำเร็จแล้ว
      var token = localStorage.getItem("accessToken");
      store.commit("SET_ACCESS", token);

      if (to.path === "/pages/login") {
        // ถ้าไปที่หน้าล็อคอินให้ไปหน้ารวมเมนู
        switch (store.state.AppActiveUser.userRole) {
          case "พนักงาน":
            next("/OrderTable");
            break;
          case "ครัว":
            next("/Kitchen");
            break;
          case "แคชเชียร์":
            next("/Cashier");
            break;
          default:
            next("/MenuForManager");
        }
      } else {
          var role = `${store.state.AppActiveUser.userRole}`;
          if(role === "หัวหน้า") role = "ผู้จัดการ"
          var secondRole = ``;
          if (role === "ผู้จัดการ") secondRole = `พนักงานและผู้จัดการ`;
          if (role === "พนักงาน") secondRole = `พนักงานและผู้จัดการ`;
          if (to.meta.rule) {
            if (!role.includes(to.meta.rule)) {
              if (!secondRole.includes(to.meta.rule)) {
                if (role === "แคชเชียร์" && to.path === "/OrderStatus") {
                  next();
                } else {
                  switch (store.state.AppActiveUser.userRole) {
                    case "พนักงาน":
                      next("/OrderTable");
                      break;
                    case "ครัว":
                      next("/Kitchen");
                      break;
                    case "แคชเชียร์":
                      next("/Cashier");
                      break;
                    default:
                      next("/MenuForManager");
                  }
                }
              } else {
                next();
              }
            } else {
              next();
            }
          } else {
            next();
          }
      }
    }
  }

});

export default router;
