import axios from "@/axios.js";

export default {
  getCashier({ commit }) {
    return new Promise((resolve, reject) => {
      axios
        .get("cashier")
        .then(response => {
          if (response.data.success) {
            // เพิ่มข้อมูลสำเร็จ
            commit("SET_CASHIER", response.data.data);
          }
          resolve(response);
        })
        .catch(err => {
          reject(err);
        });
    });
  },
  getCashierLog({ commit }, item) {
    return new Promise((resolve, reject) => {
      axios
        .get("cashier/log", {
          params: item
        })
        .then(response => {
          if (response.data.success) {
            commit("SET_CASHIER_LOG", response.data.data)
          }
          resolve(response);
        })
        .catch(error => {
          reject(error);
        });
    });
  },
  addCashier({ commit }, item) {
    return new Promise((resolve, reject) => {
      axios
        .post("cashier", { item: item })
        .then(response => {
          if (response.data.success) {
            // เพิ่มข้อมูลสำเร็จ
          }
          resolve(response);
        })
        .catch(err => {
          reject(err);
        });
    });
  },
  removeCashier({ commit }, item) {
    return new Promise((resolve, reject) => {
      axios
        .put("cashier", { item: item })
        .then(response => {
          if (response.data.success) {
            // เพิ่มข้อมูลสำเร็จ
            commit("REMOVE_CASHIER");
          }
          resolve(response);
        })
        .catch(err => {
          reject(err);
        });
    });
  }
};
