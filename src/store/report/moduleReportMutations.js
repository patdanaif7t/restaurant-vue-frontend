export default {
  SET_REPORT(state, item) {
    state.data = item;
  },
  SET_REPORT_BILL(state, item) {
    state.bill = item;
  },
  SET_REPORT_CANCEL(state, item) {
    state.cancel = item;
  }
};
