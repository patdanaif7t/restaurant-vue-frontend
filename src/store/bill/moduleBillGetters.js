export default {
  getOrder: state => _.uniqBy(state.orders, "foodName")
};
