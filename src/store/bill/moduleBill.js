import state from './moduleBillState.js'
import mutations from './moduleBillMutations.js'
import actions from './moduleBillActions.js'
import getters from './moduleBillGetters.js'

export default {
  isRegistered: false,
  namespaced: true,
  state: state,
  mutations: mutations,
  actions: actions,
  getters: getters
}

