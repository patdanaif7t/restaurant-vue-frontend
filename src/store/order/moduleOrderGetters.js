export default {
  groupOrder: state => {
    var arr2 = state.orders.menu.reduce((a, b) => {
      var i = a.findIndex(
        x => x.foodName === b.foodName && x.foodDetail === b.foodDetail
      );
      return (
        i === -1
          ? a.push({
              foodName: b.foodName,
              foodDetail: b.foodDetail,
              quantity: 1
            })
          : a[i].quantity++,
        a
      );
    }, []);
    return arr2;
  },
  groupOrderStatus: state => {
    var arr2 = state.orders.menu.menu.reduce((a, b) => {
      var i = a.findIndex(
        x => x.foodName === b.foodName && x.status === b.status
      );
      return (
        i === -1
          ? a.push({
              foodName: b.foodName,
              foodDetail: b.foodDetail,
              status: b.status,
              price: b.price,
              quantity: 1
            })
          : a[i].quantity++,
        a
      );
    }, []);
    return arr2;
  },
  getSuccess: state => {},
  getUndoneOrder: state => {}
};
