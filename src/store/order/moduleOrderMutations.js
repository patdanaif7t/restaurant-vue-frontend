export default {
  SELECT_TABLE(state, item) {
    state.orders.tableId = item;
  },
  SET_CUSTOMERNUMBER(state, item) {
    state.orders.customerNumber = item;
  },
  SET_ORDER(state, item) {
    state.orders.menu.unshift(item);
  },
  SET_BILLID(state, item) {
    state.payment.billId = item;
  },
  REMOVE_ORDER_WITH_DETAIL(state, item) {
    const ItemIndex = state.orders.menu.findIndex(
      p => p.foodName == item.foodName
    );
    state.orders.menu.splice(ItemIndex, 1);
  },
  SET_ORDER_BLANK(state) {
    state.orders.menu = [];
  },
  REMOVE_ORDER(state, time) {
    const ItemIndex = state.orders.menu.findIndex(p => p.time == time);
    state.orders.menu.splice(ItemIndex, 1);
  },
  SET_USER(state, item) {
    state.orders.user = item;
  },
  SET_ORDER_BY_STATUS(state, item) {
    state.orders.menu = item;
  },
  SORT_ORDER(state) {
    state.orders.menu.menu.sort((a, b) => {
      return a.status.localeCompare(b.status, 'th')
    });
  },
  SET_ORDER_ID(state, item) {
    state.orderId = item;
  },
  SET_ORDER_KITCHEN(state, item) {
    state.kitchens.menu = item;
  },
  SET_ORDER_HISTORY(state, item) {
    state.history.menu = item;
  },
  SET_PRINT(state, item) {
    state.print = item;
  }
};
