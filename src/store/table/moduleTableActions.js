import axios from "@/axios.js";

export default {
  addItem({ commit }, item) {
    return new Promise((resolve, reject) => {
      axios
        .post("table", { item: item })
        .then(response => {
          if (response.data.success) {
            // เพิ่มข้อมูลสำเร็จ
            commit(
              "ADD_TABLE",
              Object.assign(item, {
                _id: response.data.data._id,
                status: response.data.data.status
              })
            );
          }
          resolve(response);
        })
        .catch(error => {
          console.log(error);
          reject(error);
        });
    });
  },
  fetchTables({ commit }, options) {
    var parms = JSON.stringify(options);
    return new Promise((resolve, reject) => {
      axios
        .get(`table?options=${parms}`)
        .then(response => {
          commit("SET_TABLE", response.data);
          resolve(response);
        })
        .catch(error => {
          reject(error);
        });
    });
  },
  updateItem({ commit }, item) {
    return new Promise((resolve, reject) => {
      axios
        .put(`table/${item._id}`, { item: item })
        .then(response => {
          if (response.data.success) {
            // แก้ไขข้อมูลสำเร็จ
            commit("UPDATE_TABLE", item);
          }
          resolve(response);
        })
        .catch(error => {
          reject(error);
        });
    });
  },
  removeItem({ commit }, itemId) {
    return new Promise((resolve, reject) => {
      axios
        .delete(`table/${itemId}`)
        .then(response => {
          if (response.data.success) {
            // ลบข้อมูลสำเร็จ
            commit("REMOVE_TABLE", itemId);
          }
          resolve(response);
        })
        .catch(error => {
          reject(error);
        });
    });
  }
};
